# DCCJLAContextManager

[![CI Status](https://img.shields.io/travis/刘汤圆/DCCJLAContextManager.svg?style=flat)](https://travis-ci.org/刘汤圆/DCCJLAContextManager)
[![Version](https://img.shields.io/cocoapods/v/DCCJLAContextManager.svg?style=flat)](https://cocoapods.org/pods/DCCJLAContextManager)
[![License](https://img.shields.io/cocoapods/l/DCCJLAContextManager.svg?style=flat)](https://cocoapods.org/pods/DCCJLAContextManager)
[![Platform](https://img.shields.io/cocoapods/p/DCCJLAContextManager.svg?style=flat)](https://cocoapods.org/pods/DCCJLAContextManager)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DCCJLAContextManager is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'DCCJLAContextManager'
```

## Author

刘汤圆, review617@163.com

## License

DCCJLAContextManager is available under the MIT license. See the LICENSE file for more info.
