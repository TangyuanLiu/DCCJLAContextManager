//
//  TYAppDelegate.h
//  DCCJLAContextManager
//
//  Created by 刘汤圆 on 08/21/2018.
//  Copyright (c) 2018 刘汤圆. All rights reserved.
//

@import UIKit;

@interface TYAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
