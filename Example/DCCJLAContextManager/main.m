//
//  main.m
//  DCCJLAContextManager
//
//  Created by 刘汤圆 on 08/21/2018.
//  Copyright (c) 2018 刘汤圆. All rights reserved.
//

@import UIKit;
#import "TYAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([TYAppDelegate class]));
    }
}
