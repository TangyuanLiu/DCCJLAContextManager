#
# Be sure to run `pod lib lint DCCJLAContextManager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DCCJLAContextManager'
  s.version          = '0.1.1'
  s.summary          = 'TouchID and FaceID.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
usefor TouchID + FaceID.
                    DESC

  s.homepage         = 'https://gitee.com/TangyuanLiu/DCCJLAContextManager'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '刘汤圆' => 'review617@163.com' }
  s.source           = { :git => 'https://gitee.com/TangyuanLiu/DCCJLAContextManager.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.source_files = 'DCCJLAContextManager/Classes/**/*'
  
  # s.resource_bundles = {
  #   'DCCJLAContextManager' => ['DCCJLAContextManager/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
