//
//  DCCJ_LAContextManager.m
//  LAContext
//
//  Created by 龚欢 on 2017/11/29.
//  Copyright © 2017年 龚欢. All rights reserved.
//

#import "DCCJ_LAContextManager.h"
#import <UIKit/UIKit.h>
#import <LocalAuthentication/LocalAuthentication.h>

@interface DCCJ_LAContextManager()

@property (nonatomic, copy) DCCJ_Success success;
@property (nonatomic, copy) DCCJ_Failure failure;

@end

@implementation DCCJ_LAContextManager

+ (void)dccjOpenFingerLockOn:(UIViewController *)controller
                     success:(DCCJ_Success)success
                     failure:(DCCJ_Failure)failure {
    
    if ([controller isKindOfClass:[UIViewController class]]) {
        
        /*
        if ([DCCJ_LAContextManager is_IphoneX]) {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"您要允许\"点车成金\"使用面容ID吗？" message:@"如不允许，你将无法使用面容ID完成支付、解锁等操作" preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"不允许" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                failure(nil, kCancelDidClcik);
                return;
            }];
            
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"好" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [DCCJ_LAContextManager realOpenFinger:success failure:failure];
            }];
            
            [alertController addAction:okAction];
            [alertController addAction:cancelAction];
            
            [controller presentViewController:alertController animated:YES completion:nil];
            
        } else {
            [DCCJ_LAContextManager realOpenFinger:success failure:failure];
        }
         */
        
        [DCCJ_LAContextManager realOpenFinger:success failure:failure];
    }
}

#pragma mark - Private methods

+ (void)realOpenFinger:(DCCJ_Success)success
               failure:(DCCJ_Failure)failure {
    
    NSString *message = [DCCJ_LAContextManager is_IphoneX] ? @"面容 ID 短时间内失败多次，需要验证手机密码" : @"请把你的手指放到Home键上";
//    NSInteger deviceType = [DCCJ_LAContextManager is_IphoneX] ? kLAPolicyDeviceOwnerAuthentication : LAPolicyDeviceOwnerAuthenticationWithBiometrics;
    NSInteger deviceType = LAPolicyDeviceOwnerAuthenticationWithBiometrics;
    LAContext *laContext = [[LAContext alloc] init];
    laContext.localizedFallbackTitle = @"";
    NSError *error = nil;
    BOOL isSupport = [laContext canEvaluatePolicy:(deviceType) error:&error];
    
    if (isSupport) {
        
        [laContext evaluatePolicy:(deviceType) localizedReason:message reply:^(BOOL s, NSError * _Nullable error) {
            if (s) {
                success();
            }else {
                failure(error, kAuthorFingerOpenFailure);
            }
        }];
    }else {
//        NSError *e = [NSError errorWithDomain:@"设备未开启指纹识别" code:1024 userInfo:nil];
        failure(error, kDidNotAccessFingerAuth);
    }
}

+ (BOOL)is_IphoneX {
    return [UIScreen mainScreen].bounds.size.height == 812 ? YES : NO;
}

@end

